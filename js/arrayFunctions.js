//Arrays declarations
var a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
var b = [3, 2, 9, 3, 7, 11, 15, 13, 23, 27, 32, 35, 36, 37]
var c = [1, 3, 5, 7, 11, 13, 15, 17, 19, 21, 23, 27, 35, 37]
var d = [9, 17, 32, 7, 2, 3, 1, 45, 46, 47, 55, 60, 6, 17]
var results = [];
/**
 * Function to print current arrays values on text area
 * @void
 */
function printArrays() {
    this.clearTextbox();
    document.getElementById("textbox").value += "a -> [" + a.valueOf() + "]\n";
    document.getElementById("textbox").value += "b -> [" + b.valueOf() + "]\n";
    document.getElementById("textbox").value += "c -> [" + c.valueOf() + "]\n";
    document.getElementById("textbox").value += "d -> [" + d.valueOf() + "]\n";
    document.getElementById("textbox").value += "\nresultados -> [" + results.sort((a, b) => a - b).valueOf() + "]\n";

}
/**
 * Function to search common elements between the arrays
 * @void
 */
function searchOnArrays() {
    for (var i = 0; i <= a.length; i++) { //Check if first array have some common inputs whit other arrays.
        if (b.indexOf(a[i]) >= 0) {
            if (results.indexOf(a[i]) === -1) {
                results.push(a[i]);
                this.removeItems(b, a[i]); //Remove common item coincidence
            } else {
                this.removeItems(b, a[i]); //Remove common item coincidence
            }
        }
        if (c.indexOf(a[i]) >= 0) {
            if (results.indexOf(a[i]) === -1) {
                results.push(a[i]);
                this.removeItems(c, a[i]); //Remove common item coincidence
            } else {
                this.removeItems(c, a[i]); //Remove common item coincidence
            }
        }
        if (d.indexOf(a[i]) >= 0) {
            if (results.indexOf(a[i]) === -1) {
                results.push(a[i]);
                this.removeItems(d, a[i]); //Remove common item coincidence
            } else {
                this.removeItems(d, a[i]); //Remove common item coincidence
            }
        }
    }
    for (var i = 0; i <= b.length; i++) { //Check if second array have some common inputs whit other arrays.
        if (c.indexOf(b[i]) >= 0) {
            if (results.indexOf(b[i]) === -1) {
                results.push(b[i]);
                this.removeItems(c, b[i]); //Remove common item coincidence
            } else {
                this.removeItems(c, b[i]); //Remove common item coincidence
            }
        }
        if (d.indexOf(b[i]) >= 0) {
            if (results.indexOf(b[i]) === -1) {
                results.push(b[i]);
                this.removeItems(d, b[i]); //Remove common item coincidence
            } else {
                this.removeItems(d, b[i]); //Remove common item coincidence
            }
        }
    }
    for (var i = 0; i <= c.length; i++) { //Check if third array have some common inputs whit other arrays.
        if (d.indexOf(c[i]) >= 0) {
            if (results.indexOf(c[i]) === -1) {
                results.push(c[i]);
                this.removeItems(d, c[i]); //Remove common item coincidence
            } else {
                this.removeItems(d, c[i]); //Remove common item coincidence
            }
        }
    }
    this.printArrays();
}
/**
 * Function to remove common items inside other arrays
 * @param  {array,value}
 */
function removeItems(array, value) {
    for (var i = 0; i <= array.length; i++) {
        if (array[i] === value) {
            array.splice(array.indexOf(value), 1);
        }
    }
}
/**
 * Function for clear text area
 * @void
 */
function clearTextbox() {
    document.getElementById("textbox").value = "";
}
/**
 * Function to load original values in arrays
 * @void
 */
function reloadArrays() {
    a = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15]
    b = [3, 2, 9, 3, 7, 11, 15, 13, 23, 27, 32, 35, 36, 37]
    c = [1, 3, 5, 7, 11, 13, 15, 17, 19, 21, 23, 27, 35, 37]
    d = [9, 17, 32, 7, 2, 3, 1, 45, 46, 47, 55, 60, 6, 17]
    results = [];
}