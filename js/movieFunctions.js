//Array of objects declaration
var movies = [
    { title: "Snatch", starring: "Brad Pitt, Jason Statham, Benicio del Toro, Vinnie Jones, Sephen Graham", isFavorite: true, score: 9 },
    { title: "Lock, Stock and Two Smoking Barrels", starring: "Jason Statham, Dexter Fletcher, Vinnie Jones, Jason Flemyng, Nick Moran", isFavorite: true, score: 9.5 },
    { title: "Hero", starring: "Jet Li, Maggie Ceung, Tony Leung, Chang Ziyi, Donnie Yen", isFavorite: true, score: 8.5 },
    { title: "In Time", starring: "Amanda Seyfried, Justin Timberlake, Cillian Murphy, Olvicia Wilde, Matt Bomer", isFavorite: false, score: 8 },
    { title: "Central Intelligence", starring: "Dwayne Johnson, Kevin Hart, Rawson Marshal Thurber, Danielle Nocolet, Aron Paul", isFavorite: false, score: 7 },
    { title: "Kill Bill", starring: "Uma Thirman, David Carradine, Daryl Hannah, Lucy Lui, Michael Madsen", isFavorite: false, score: 9.5 },
    { title: "National Security", starring: "Martin Lawrence, Steve Zahn, Robinne Lee, Mari Morrow, Bill Duke", isFavorite: true, score: 7.5 },
    { title: "Men in Black III", starring: "Will Smith, Tommy Lee Jones, Josh Brolin, Emma Thompson, Jemaine Clement", isFavorite: false, score: 8.5 },
    { title: "The Truman Show", starring: "Jim Carrey, Ed Harris, Laura Linney, Natascha McElhone, Noah Emmeri", isFavorite: true, score: 6.5 },
    { title: "Pleasantville", starring: "Tobey Maguire, Resse Witherspoon, William H. Macy, Joan Allen, Jeff Daniels", isFavorite: false, score: 9 }
];
/**
 * Function to sort movies list
 * @param  {order, direction}
 */
function sortMovies(order, direction) { //Function to order array of movies and after show sort result in text area
    this.clearTextbox();
    if (order === 'title') {
        if (direction === 'asc') {
            movies.sort(function(a, b) {
                var _a = a.title.toLowerCase();
                var _b = b.title.toLowerCase();
                return _a < _b ? -1 : _a > _b ? 1 : 0; //Order title alphabetically
            });
        } else if (direction === 'desc') {
            movies.sort(function(a, b) {
                var _a = a.title.toLowerCase();
                var _b = b.title.toLowerCase();
                return _b < _a ? -1 : _b > _a ? 1 : 0; //Order title alphabetically
            });
        }
    }
    if (order === 'score') {
        if (direction === 'asc') {
            movies.sort(function(a, b) {
                var _a = a.score;
                var _b = b.score;
                return _a < _b ? -1 : _a > _b ? 1 : 0; //Order by first lowest score
            });
        } else if (direction === 'desc') {
            movies.sort(function(a, b) {
                var _a = a.score;
                var _b = b.score;
                return _b < _a ? -1 : _b > _a ? 1 : 0; //Order by first highest score
            });
        }
    }
    if (order === 'isFavorite') {
        if (direction === 'asc') {
            movies.sort(function(a, b) {
                var _a = a.isFavorite;
                var _b = b.isFavorite;
                return _a === _b ? 0 : _a ? -1 : 1; //Order by first all no favorites
            });
        } else if (direction === 'desc') {
            movies.sort(function(a, b) {
                var _a = a.isFavorite;
                var _b = b.isFavorite;
                return _a === _b ? 0 : _a ? 1 : -1; //Order by first all favorites
            });
        }
    }
    movies.forEach(printMovies);
}
/**
 * Function to print movie list in textarea
 * @void
 */
function printMovies(movie, index) {
    document.getElementById("textbox").value += "* " + movie.title + " starring " + movie.starring + " has a score of " + movie.score + '\n\n';
}