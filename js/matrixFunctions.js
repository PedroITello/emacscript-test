//Variables declaration
var _rows = 0;
var _columns = 0;
var matrix = [];

/**
 * Function to create matrix
 * @param  {rows, columns, elements}
 */
function createMatrix(rows, columns, elements) {
    this.clearTextbox();
    _rows = rows;
    _columns = columns;
    var elementsArray = elements.split(",");
    var matrixElements = rows * columns;
    if (elementsArray.length === matrixElements) { //Check if elementsArray have enough elements to populate matrix acording rows * columns value
        var rowElements = 0;
        for (var i = 0; i < rows; i++) {
            matrix[i] = [];
            for (var j = 0; j < columns; j++) {
                matrix[i][j] = elementsArray[rowElements]; //Add values to matrix;
                rowElements++;
            }
            document.getElementById("textbox").value += matrix[i].valueOf() + "\n"; //Print matrix on text area
        }
    } else {
        document.getElementById("textbox").value = "El tama\u00F1o de la matr\u00CDz no coincide con la cantidad de elementos asignados";
    }
}
/**
 * Function to serach zero inside a matrix element
 * @void
 */
function searchZero() {
    for (var i = 0; i < _rows; i++) {
        for (var j = 0; j < _columns; j++) {
            if (matrix[i][j] === "0") { //Check if element is 0
                this.clearTextbox();
                this.putZero(i, j); //Call function to populate whit 0 in the row and column of the coincidence
                return;
            }
        }
    }
}
/**
 * Function to put 0 elements in the row and column of the 0 coincidence
 * @param  {row, column}
 */
function putZero(row, column) { //Fill spaces in rows and columns whit 0
    for (var i = 0; i < _columns; i++) {
        matrix[row][i] = "0";
    }
    for (var i = 0; i < _rows; i++) {
        matrix[i][column] = "0";
        document.getElementById("textbox").value += matrix[i].valueOf() + "\n";
    }
}