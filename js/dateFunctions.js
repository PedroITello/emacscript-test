/**
 * Function to calculate the diference between two dates in minutes
 * @param  {date1,date2}
 */
function diferenceDates(date1, date2) {
    if ((Object.prototype.toString.call(this.parseDate(date1)) === "[object Date]" && Object.prototype.toString.call(this.parseDate(date1)) === "[object Date]")) { //Check if parseDate return a correct date type
        var _date1 = this.parseDate(date1); //Call parseDate(date) function
        var _date2 = this.parseDate(date2);
        if (_date1 !== false && _date2 !== false) { //Check if date is created on parseDate(date) function
            var diference = Math.abs(_date1.getTime() - _date2.getTime()); //getTime() return time in miliseconds
            var result = Math.floor(diference / 60000); //divide by 60000 to obtain time in minutes
            document.getElementById("result").value = result; //Show result
        }
    } else {
        document.getElementById("result").value = "Formato de fecha requerido: dd/mm/aaaa";
    }
}
/**
 * Function to parse date from input format to date format
 * @param  {date1,date2}
 * @return  {Date||boolean}
 */
function parseDate(date) {
    var newDate = date.match(/(\d+)/g); //Return date as array
    if (newDate.length === 3) {
        if ((newDate[0].length > 0 && newDate[0].length <= 2) &&
            (newDate[1].length > 0 && newDate[1].length <= 2) &&
            (newDate[1] <= 12) && (newDate[0] <= 31)) { //Validate format dd/mm/aaaa
            return new Date(newDate[2], newDate[1] - 1, newDate[0]); //Return new date after parse it
        } else {
            return false;
        }
    } else {
        return false;
    }
}