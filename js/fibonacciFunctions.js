//Variables declaration
var a = 1;
var b = 0;
var temp;
var count = 0;
/**
 * Function to print fibonacci series from first 100 numbers
 * @void
 * @return {fibonacci}
 */
function fibonacci() {
    this.clearTextbox();
    while (count < 100) { //Stop at first 100 elements
        temp = a;
        a = a + b;
        b = temp; //Asign fibonacci value in iteration
        count++;
        document.getElementById("textbox").value += count + " -> " + b + "\n";
    }

    return b;
}