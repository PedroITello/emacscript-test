//Variables declaration
var decimalValue = [1000, 900, 500, 400, 100, 90, 50, 40, 10, 9, 5, 4, 1];
var romanNumeral = ["M", "CM", "D", "CD", "C", "XC", "L", "XL", "X", "IX", "V", "IV", "I"];
var romanized = "";
/**
 * Function to transform and print a decimal value to roman numeral
 * @void
 */
function convertToRoman(number) {
    romanized = "";
    if (number >= 1 && number <= 3999) { //Check if number is inside 1 to 3999

        for (var i = 0; i < decimalValue.length; i++) {
            while (decimalValue[i] <= number) { //Repeat symbols according the input number
                romanized += romanNumeral[i]; //Add roman symbols to representate the number to transform
                number -= decimalValue[i]; //Remove symbol equivalent to decimal values
            }
        }

        document.getElementById("result").value = romanized;
    } else {
        document.getElementById("result").value = "Ingrese un n\u00FAmero v\u00E1lido entre 1 y 3999";
    }
}