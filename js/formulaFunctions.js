//Variables declaration
var computed = 0;
/**
 * Function to compute declared Formula
 * @void
 */
function computeForumla() {
    for (var k = 0; k <= Math.pow(10, 6); k++) { // From k = 1 to 10^6
        computed += Math.pow(-1, k + 1) / (2 * k) - 1; // (-1)^k+1 / 2(k) - 1
    }
    document.getElementById("textbox").value = 4 * computed;
}